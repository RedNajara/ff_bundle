$(document).ready(function() {
  $('#fullpage').fullpage({
    'verticalCentered': false,
    'css3': true,
    'navigation': false
  });

  function MouseWheelHandler(e) {
      e = window.event || e;
      var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.deltaY || -e.detail)));
      SlideSection(delta);
    return false;
  }

  function SlideSection(delta) {
    // number current section
    var section = $('.fp-section.active').attr('id').replace('section', '');
  }

  $(document).keydown(function(e) {
    //Moving the main page with the keyboard arrows if keyboard scrolling is enabled
      switch(e.which) {
        //up
        case 38:
        case 33:
          SlideSection(1);
          break;

        //down
        case 40:
        case 34:
          SlideSection(-1);
          break;
        default:
          return; // exit this handler for other keys
      }
  });

});
